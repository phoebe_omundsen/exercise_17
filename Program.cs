﻿using System;

namespace exercise_17
{
    class Program
    {
        static void Main(string[] args)
        {
            //clear noise
            Console.Clear();
            //Welcome user to app
            Console.WriteLine("========================================");
            Console.WriteLine("====   Hello Welcome to my App :)   ====");
            Console.WriteLine("========================================");
            Console.WriteLine();
            //Ask user to make a decision and input this
            Console.WriteLine("Do you like Cats? True=Yes/False=No");
            var answer1 = bool.Parse(Console.ReadLine());

            //if statement is true, display message to screen
            if (answer1)
            {
                Console.WriteLine("Congratulations, you have won 1 Billion, Trillion Dollars!");
            }
            //if statement is false, display message to screen
            if (!answer1)
            {   Console.WriteLine("SHUN THE NON BELIEVER, SHUUUUUNNNN!");
                Console.WriteLine("The tribe has spoken. You have been eliminated from the game :(");
            }
            Console.WriteLine("Press any key to End =)");
            Console.ReadKey();
        }
    }
}
